package sheridan;

import static org.junit.Assert.*;

/**
 * Author: Sam Whelan
 * Student ID: 991541458
 */

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void testFromFahrenheitRegular() {
		int celsius = Celsius.fromFahrenheit(32);
		assertTrue("Not a valid Celsius response", celsius == 0);
	}
	
	@Test
	public void testFromFahrenheitException() {
		int celsius = Celsius.fromFahrenheit(32);
		System.out.println(celsius);
		assertFalse("Not a valid Celsius response", celsius != 0.0);
	}
	
	@Test
	public void testFromFahrenheitBoundaryIn() {
		int celsiusA = Celsius.fromFahrenheit(1);
		int celsiusB = Celsius.fromFahrenheit(2);
		
		assertTrue("Not a valid Celsius response", celsiusA != celsiusB);
	}
	
	@Test
	public void testFromFahrenheitBoundaryOut() {
		int celsiusA = Celsius.fromFahrenheit(1);
		int celsiusB = Celsius.fromFahrenheit(2);		
		assertFalse("Not a valid Celsius response", celsiusA == celsiusB);
	}
	


}
