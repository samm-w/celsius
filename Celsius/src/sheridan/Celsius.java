package sheridan;

public class Celsius {
	
	public static int fromFahrenheit(int argument) {
		double celsius = (( 5 *(argument - 32.0)) / 9.0);		
		return (int) celsius;
	}
}
